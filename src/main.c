#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

static void* HEAP;

void test1() {
	fprintf(stdout, "\nTest #1. Allocating one block\n");
	
	size_t block_size = 100;
	
	fprintf(stdout, "\nHeap before allocation\n");
	debug_heap(stdout, HEAP);
	
	fprintf(stdout, "\nAllocation (size: %zu)\n", block_size);
	_malloc(sizeof(uint8_t) * block_size);
	
	fprintf(stdout, "\nHeap after allocation\n");
	debug_heap(stdout, HEAP);
	
	fprintf(stdout, "\nEnd of test\n");
}

void test2() {
	fprintf(stdout, "\nTest #2. releasing one block\n");
	
	size_t block_size1 = 200;
	size_t block_size2 = 250;
	
	fprintf(stdout, "\nallocations (sizes: %zu, %zu)\n", block_size1, block_size2);
	void* addr = _malloc(sizeof(uint8_t) * block_size1);
	_malloc(sizeof(uint8_t) * block_size2);
	
	fprintf(stdout, "\nHeap before releasing one block\n");
	debug_heap(stdout, HEAP);
	
	_free(addr);
	
	fprintf(stdout, "\nHeap after releasing one block\n");
	debug_heap(stdout, HEAP);
	
	fprintf(stdout, "\nEnd of test\n");
}

void test3() {
	fprintf(stdout, "\nTest #3. releasing few blocks\n");
	
	size_t block_size1 = 300;
	size_t block_size2 = 350;
	
	fprintf(stdout, "\nAllocation (sizes: %zu, %zu)\n", block_size1, block_size2);
	void* addr1 = _malloc(sizeof(uint8_t) * block_size1);
	void* addr2 = _malloc(sizeof(uint8_t) * block_size2);
	
	fprintf(stdout, "\nHeap before releasing few blocks\n");
	debug_heap(stdout, HEAP);
	
	_free(addr1);
	_free(addr2);
	
	fprintf(stdout, "\nHeap after releasing few blocks\n");
	debug_heap(stdout, HEAP);
	
	fprintf(stdout, "\nEnd of test\n");
}

void test4() {
	fprintf(stdout, "\nTest #4. Mapping another region\n");
	
	size_t block_size = 12000;
	
	fprintf(stdout, "\nHeap before mapping another region next to last block\n");
	debug_heap(stdout, HEAP);
	
	fprintf(stdout, "\nAllocating huge block (size: %zu)\n", block_size);
	_malloc(sizeof(uint8_t) * block_size);
	
	fprintf(stdout, "\nHeap after mapping another region\n");
	debug_heap(stdout, HEAP);
	
	fprintf(stdout, "\nEnd of test\n");
}

void test5() {
	fprintf(stdout, "\nTest #5. Mapping region in another place\n");
	
	const size_t block_size = 40000;
	
	struct block_header* tmp_block = HEAP;
	struct block_header* last = NULL;
	
	while (tmp_block != NULL) {
		last = tmp_block;
		tmp_block = tmp_block->next;
	}
	
	if (last != NULL) {	
		fprintf(stdout, "\nMapping address of the next block to last");
			
		void* new_addr = (void*) (last->contents + last->capacity.bytes);
		new_addr = mmap(new_addr, 1000, PROT_WRITE | PROT_READ, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
		
		fprintf(stdout, "\nMapped address: %p.\n", new_addr);
		
		fprintf(stdout, "\nHeap before mapping another region\n");
		debug_heap(stderr, HEAP);
		
		fprintf(stdout, "\nAllocating huge block (size: %zu)\n", block_size);
		void* addr = _malloc(block_size);
		
		fprintf(stdout, "\nHeap after mapping another region\n");
		debug_heap(stderr, HEAP);
		
		fprintf(stdout, "\nreleasing huge block (size: %zu)\n", block_size);
		_free(addr);
		
		fprintf(stdout, "\nHeap after releasing huge block\n");
		debug_heap(stderr, HEAP);
		
		fprintf(stdout, "\nEnd of test\n");
	}
}

int main() {
	HEAP = heap_init(REGION_MIN_SIZE);
	
	test1();
	test2();
	test3();
	test4();
	test5();

	return 0;
}
